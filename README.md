# pi-firesoundbar-endpoint



## What is this

Golang toggle daemon for my raspberry pi to turn soundbar on and off and launch project outfox (dance game), can use it to launch other programs as well or modify it to just be a toggle endpoint.

## License
This project is licensed under the AGPLv3 License - see the LICENSE file for details.