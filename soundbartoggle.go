// soundbartoggle.go
// Copyright (C) 2023 Aaron Steenkamer
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

package main

import (
    "bufio"
    "bytes"
    "fmt"
    "log"
    "net/http"
    "os/exec"
    "strings"
    "time"
)

const soundbarMAC = "XX:XX:XX:XX:XX:XX"
const outFoxPath = "/home/pi/OutFox/OutFox"

func sendBluetoothCmd(args ...string) error {
    cmd := exec.Command("bluetoothctl", args...)
    return cmd.Run()
}

func isSoundbarConnected() (bool, error) {
    cmd := exec.Command("bluetoothctl", "info", soundbarMAC)
    output, err := cmd.CombinedOutput()
    if err != nil {
        return false, fmt.Errorf("error checking soundbar status: %s", err)
    }

    scanner := bufio.NewScanner(bytes.NewReader(output))
    for scanner.Scan() {
        if strings.Contains(scanner.Text(), "Connected: yes") {
            return true, nil
        }
    }
    return false, nil
}

func launchOutFox() error {
    cmd := exec.Command(outFoxPath)
    cmd.Env = append(cmd.Env, "DISPLAY=:0")
    if err := cmd.Start(); err != nil {
        return fmt.Errorf("failed to start OutFox: %s", err)
    }
    return nil
}

func killOutFox() error {
    cmd := exec.Command("pkill", "-f", outFoxPath)
    if err := cmd.Run(); err != nil {
        return fmt.Errorf("failed to stop OutFox: %s", err)
    }
    return nil
}

func connectSoundbar(w http.ResponseWriter, r *http.Request) {
    isConnected, err := isSoundbarConnected()
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }
    if isConnected {
        fmt.Fprintf(w, "Soundbar already connected")
        return
    }

    err = sendBluetoothCmd("connect", soundbarMAC)
    if err != nil {
        http.Error(w, fmt.Sprintf("Failed to connect to soundbar: %s", err), http.StatusInternalServerError)
        return
    }

    time.Sleep(5 * time.Second) // Wait to ensure the soundbar is connected

    err = launchOutFox()
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }

    fmt.Fprintf(w, "Soundbar connected and OutFox launched")
}

func disconnectSoundbar(w http.ResponseWriter, r *http.Request) {
    err := killOutFox() // Ensure OutFox is stopped before disconnecting the soundbar
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }

    err = sendBluetoothCmd("disconnect", soundbarMAC)
    if err != nil {
        http.Error(w, fmt.Sprintf("Failed to disconnect soundbar: %s", err), http.StatusInternalServerError)
        return
    }

    fmt.Fprintf(w, "OutFox stopped and soundbar disconnected")
}

func main() {
    http.HandleFunc("/connect", connectSoundbar)
    http.HandleFunc("/disconnect", disconnectSoundbar)

    fmt.Println("Starting server on :8080...")
    log.Fatal(http.ListenAndServe(":8080", nil))
}
